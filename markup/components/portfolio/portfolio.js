import Component from 'helpers-js/Component';
import 'owl.carousel';

export default Component.create('portfolio', class {
    constructor($block) {
        this.$block = $block;
        this.init();
    }

    init() {
        let owlPortfolio = document.querySelector('.portfolio');
        let $owlPortfolioSliders = $(owlPortfolio).find('.owl-slider');

        $owlPortfolioSliders.each(function () {
            $owlPortfolioSliders.owlCarousel({
                items: 1,
                margin: 10,
                autoWidth: true,
                pagination: false,
                pullDrag: true,
                loop: true,
                mouseDrag: true,
                dots: false,
                nav: false,
                center: true
            });
        });
        // $('#owl-portfolio1').owlCarousel({
        //     margin: 0,
        //     pagination: false,
        //     pullDrag: true,
        //     mouseDrag: true,
        //     dots: true,
        //     nav: true,
        //     center: true,
        //     navContainer: '.portfolio__nav',
        // });
    }
});
