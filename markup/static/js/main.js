'use strict';

import polyfills from './libraries/polyfills';
import Component from 'helpers-js/Component';

import 'components/scrollto/scrollto';
import 'components/slide-menu/slide-menu';
import 'components/ui-slide/ui-slide';
import 'components/select/select';
import 'components/input/input';
import 'components/popup/popup';
import 'components/accordion-pl/accordion-pl';

$(() => {
    polyfills.init();
    Component.initAll();
    // ================ Здесь инициализируем модули =====================
});
